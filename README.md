
Build FreeRADIUS from source
============================

1) change name, email in debian changelog - part of this repo. Those will be used for sigining.  

2) Download artifacts  

3) cd to the extracted freeradius directory and create .chagnes file:  
dpkg-genchanges -S > ../freeradius_3.0.X+git~ppa1~ubuntu16.04_source.changes

4) sign .changes file with debsign:  
debsign -k example@email.com freeradius_3.0.X+git.dsc

5) upload files using dput (sftp):  
dput radius-ppa freeradius_3.0.X+git~ppa1~ubuntu16.04_source.changes


If you want to use your own PPA repo, you have to register and initialize it.
`dput` config should have section like this:  

```
vim ~/.dput.cf

[radius-ppa]
fqdn = ppa.launchpad.net
method = sftp
incoming = ~viralpoetry/ubuntu/freeradius3
login = viralpoetry
allow_unsigned_uploads = 0
```